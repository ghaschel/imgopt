@echo off
title Optimizando JPGs
for /R %%v in (img\*.jpg) do (
  jpegtran.exe -copy none -optimize -progressive -arithmetic -verbose %%v %TMP%\%%~nv
  copy %TMP%\%%~nv %%v
  del %TMP%\%%~nv
)
for /R %%v in (img\*.jpeg) do (
  jpegtran.exe -copy none -optimize -progressive -arithmetic -verbose %%v %TMP%\%%~nv
  copy %TMP%\%%~nv %%v
  del %TMP%\%%~nv
)
title Optimizando GIFs
for /R %%v in (img\*.gif) do gifsicle.exe -b --verbose --interlace --no-comments --no-names --no-extensions -O3 %%v
title Optimizando PNGs
for /R %%v in (img\*.png) do pngout.exe /k0 /s0 /y %%v
for /R %%v in (img\*.png) do (
  pngcrush.exe -v -rem gAMA -rem cHRM -rem iCCP -rem sRGB -brute -l 9 %%v %TMP%\%%~nv
  copy %TMP%\%%~nv %%v
  del %TMP%\%%~nv
)
for /R %%v in (img\*.png) do optipng.exe -v -o7 -strip all -clobber -f5 -i0 -zc9 -zm9 -zs3 %%v
for /R %%v in (img\*.png) do deflopt.exe /v %%v
pause
