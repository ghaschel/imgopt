:engine_19092015
@echo off
setlocal enabledelayedexpansion
:user_settings
set "autorun=0"
set "autorun_option=0"
set "auto_close=0"
:script
set "name=ScriptPNG"
set "version=01.11.2015"
if "%~1" equ "thread" call:thread_work %4 %5 "%~2" "%~3" & exit /b
if "%~1" equ "" call:how_to & exit /b
title %name% - %version%
color 0f
:script_configuration
set "script_name=%~0"
set "path_source=%~dp0"
set "script_config=%path_source%lib\"
set "path_scripts=%path_source%lib\"
set "temporary_path=%temp%\%name%\"
set "time_wait=30"
set "script_lock=%temp%\script.lock"
set "separe_bar=______________________________________________________________________________"
set "check_file=%path_source%\%name%"
:script_check_run
set "already_run="
call:already_run "%name% - %version%"
if defined already_run (
exit
)
set "check_file=%check_file%%already_run%"
if not defined already_run if exist "%temporary_path%" 1>nul 2>&1 rd /s /q "%temporary_path%"
:script_lib_path
set "lib=%~dp0lib\"
path %lib%;%path%
:script_check_files
set "no_file="
if not exist %lib% set "no_file=1"
if not exist %lib%advdef.exe set "no_file=1"
if not exist %lib%deflopt.exe set "no_file=1"
if not exist %lib%pngquant.exe set "no_file=1"
if not exist %lib%pngwolf.exe set "no_file=1"
if not exist %lib%truepng.exe set "no_file=1"
if not exist %lib%filter-png.js set "no_file=1"
:file_list_error
if defined no_file (
cls
title %name% - %version%
if exist "%temporary_path%" 1>nul 2>&1 rd /s /q "%temporary_path%"
1>&2 echo.
1>&2 echo.
1>&2 echo. %name% can not find lib folder or files.
1>&2 echo.
1>&2 echo.
call:script_pause
exit /b
)
:temporary_settings
set "random_number=%random%"
1>nul 2>&1 md "%temporary_path%%random_number%"
:script_counters
for %%a in (PNG) do (
set "image_number%%a=0"
set "total_number%%a=0"
set "total_number_error%%a=0"
set "total_size%%a=0"
set "image_size%%a=0"
set "change_size%%a=0"
set "change_purcent%%a=0"
)
:script_parameters
set "png="
set "start_time="
set "finish_time="
set "log_file=%temporary_path%\files"
set "png_counters=%temporary_path%\png_number"
set "file_list=%temporary_path%\file_list"
set "file_list_error=%temporary_path%\file_error"
set "thread="
:set_thread
call:set_number_thread %thread%
call:scan_files %*
if "%png%" equ "0" goto:no_images_found
:first_echo
cls
title %name% - %version%
cscript //nologo //e:jscript "%path_scripts%filter-png.js" /png:1 %* 1>"%file_list%" 2>"%file_list_error%"
:set_counters
if exist "%file_list%" (
if "%png%" neq "0" for /f "tokens=*" %%a in ('findstr /i /e ".png"  "%file_list%" ^| find /i /c ".png" 2^>nul') do set /a "total_numberPNG+=%%a"
)
if %total_numberPNG% gtr 0 (if not defined png call:png) else set "png=0"
:no_images_found
if %total_numberPNG% equ 0 (
cls
1>&2 echo.
1>&2 echo.
1>&2 echo. %name% can not find images.
1>&2 echo.
1>&2 echo.
call:script_pause
exit /b
)
for /l %%a in (1,1,%thread%) do (
>"%log_file%png.%%a" echo.
)
:first_echo
cls
1>&2 echo.
1>&2 echo.
1>&2 echo. %name% - %version%
1>&2 echo.
1>&2 echo.
:set_title-time
call:set_title
call:set_time start_time
for /f "usebackq tokens=1,2 delims=	" %%a in ("%file_list%") do (
call:initialise_source "%%~a"
if defined ispng if "%png%" neq "0" call:file_work "%%~fa" "%%~fb" png %thread% image_numberPNG
)
:thread_wait
call:wait_flag "%temporary_path%\thread*.lock"
for /l %%z in (1,1,%thread%) do (
call:type_log png %%z
)
call:set_title
call:set_operation
call:script_pause
exit /b
:scan_files
if "%~1" equ "" exit /b
set "total_all=%~1"
if "!total_all:~,1!" equ "/" (
if /i "!total_all:~1,4!" equ "PNG:" (
set "png=!total_all:~5!"
if "!png!" neq "0" if "!png!" neq "1" if "!png!" neq "2" (
set "png=0"
)
else if /i "!total_all:~1,7!" equ "" (
set "outdir=!total_all:~8!"
)
)
shift
goto:scan_files
:check_if_running
call:already_run "%~1"
if defined already_run (
exit
)
exit /b
:already_run
set "already_run="
for /f "tokens=* delims=" %%a in ('tasklist /fo csv /v /nh ^| find /i /c "%~1" ') do (
if %%a gtr 1 set "already_run=%%a"
)
exit /b
:set_time
set "%1=%time:~0,2%:%time:~3,2%:%time:~6,2%"
exit /b
:create_thread
if %2 equ 1 call:thread_work %1 1 %3 %4 & call:type_log %1 1 & exit /b
for /l %%z in (1,1,%2) do (
if not exist "%temporary_path%\thread%%z.lock" (
call:type_log %1 %%z
>"%temporary_path%\thread%%z.lock" echo. Processing: %3
start /b /low /min cmd.exe /s /c ""%script_name%" thread "%~3" "%~4" %1 %%z"
exit /b
)
)
call:wait_random 500
goto:create_thread
:type_log
if %thread% equ 1 exit /b
if not defined type_number%1%2 set "type_number%1%2=1"
call:type_log_file "%log_file%%1.%2" "type_number%1%2" %%type_number%1%2%% %1
exit /b
:type_log_file
if not exist "%~1" exit /b
for /f "usebackq skip=%3 tokens=1-5 delims=;" %%b in ("%~1") do (
if /i "%%d" equ "error" (
call:echo_file_error "%%~b" "%%~c"
) else (
call:echo_file_info "%%~b" %%c %%d %%e %%f
)
set /a "%~2+=1"
)
exit /b
:echo_file_info
call:echo_script " %~n1.png"
if "%~4" neq "0" (
set "float=%2"
call:division float 1024 100
call:echo_script " In  : !float! KB"
set "change_size=%4"
call:division change_size 1024 100
set "float=%3"
call:division float 1024 100
call:echo_script " Out : !float! KB (%5%%%%%%)"
) else (
set "float=%2"
call:division float 1024 100
call:echo_script " In  : !float! KB"
set "change_size=%4"
call:division change_size 1024 100
set "float=%3"
call:division float 1024 100
call:echo_script " Out : !float! KB (%5%%%%%%) - skipped"
)
call:echo_script %separe_bar%
call:echo_script
exit /b
:echo_file_error
call:echo_error " In    : %~n1.png"
call:echo_error " Error : %~2"
call:echo_error " %separe_bar%"
call:echo_error 
exit /b
:echo_script
echo.%~1
exit /b
:echo_error
1>&2 echo.%~1
exit /b
:thread_work
1>nul 2>&1 md "%~dp4"
if /i "%1" equ "png" call:pngfile_work %2 %3 %4 & if %thread% gtr 1 >>"%png_counters%.%2" echo.1
if exist "%temporary_path%\thread%2.lock" >nul 2>&1 del /f /q "%temporary_path%\thread%2.lock"
exit /b
:wait_flag
if not exist "%~1" exit /b
call:wait_random 2000
goto:wait_flag
:wait_random
set /a "wait_ms=%random%%%%1"
1>nul 2>&1 ping -n 1 -w %wait_ms% 127.255.255.255
exit /b
:initialise_source
set "ispng="
if /i "%~x1" equ ".png" set "ispng=1"
exit /b
:set_number_thread
set "thread=4"
exit /b
:png
if %autorun_option% neq 1 if %autorun_option% neq 2 if %autorun_option% neq 3 if %autorun_option% neq 4 if %autorun_option% neq 5 if %autorun_option% neq 8 if %autorun_option% neq 9 set "autorun_option=1"
if %autorun% equ 1 set "png=%autorun_option%" & exit /b
cls
set "png="
title %name% - %version%
1>&2 echo.
1>&2 echo.
1>&2 echo.
1>&2 echo. 
1>&2 echo.   Lossless for Web :
1>&2 echo.   ----------------
1>&2 echo.
1>&2 echo.   [1] Fastest     [2] Fast     [3] Intense     [4] High     [5] Best
1>&2 echo.
1>&2 echo.
1>&2 echo.   Lossy conversion :
1>&2 echo.   ----------------
1>&2 echo.
1>&2 echo.   [8] PNG8+A      [9] PNG24+A
1>&2 echo.
1>&2 echo.
1>&2 echo.
1>&2 echo.
set /p png="--> Choose an option : "
1>&2 echo.
if "%png%" neq "1" if "%png%" neq "2" if "%png%" neq "3" if "%png%" neq "4" if "%png%" neq "5" if "%png%" neq "8"  if "%png%" neq "9" goto:png
exit /b
:set_title
if "%png%" equ "0" (title %~1%name% - %version% & exit /b)
if %thread% gtr 1 (
set "image_numberPNG=0"
for /l %%c in (1,1,%thread%) do  (
for %%b in ("%png_counters%.%%c") do set /a "image_numberPNG+=%%~zb/3" 2>nul
)
)
set "title_progression="
set /a "change_purcent=%image_numberPNG%*100/%total_numberPNG%"
set "title_progression=!change_purcent!%%"
title %title_progression% - %name% - %version%
exit /b
:file_work
call:create_thread %3 %4 "%~f1" "%~f2"
set /a "%5+=1"
call:set_title
exit /b
:pngfile_work
set "zlib_memlevel="
set "zlib_strategy="
set "pngoptimized_size=%~z2"
set "error_backup=0"
set "log_file2=%log_file%png.%1"
set "png_log=%temporary_path%\png%1.log"
set "file_work=%temporary_path%%~n2-script%1%~x2"
set "temp_file=%temporary_path%%random%%~nx2"
if not exist "%~2" (
call:save_error_log "%~f2" "can not read image"
exit /b 1
)
if %png% equ 1 (
1>nul 2>&1 truepng -i0 -g0 -md remove all -quiet -force -y -out "%file_work%" "%~2"
)
if %png% equ 2 (
1>nul 2>&1 truepng -a2 -i0 -g0 -md remove all -quiet -force -y -out "%file_work%" "%~2"
)
if %png% equ 3 (
1>nul 2>&1 truepng -i0 -g0 -md remove all -quiet -force -y -out "%file_work%" "%~2"
advdef -z -3 -q "%file_work%"
)
if %png% equ 4 (
1>nul 2>&1 truepng -a2 -i0 -g0 -md remove all -quiet -force -y -out "%file_work%" "%~2"
advdef -z -3 -q "%file_work%"
)
if %png% equ 5 (
>"%png_log%" 2>nul truepng -a2 -i0 -g0 -md remove all -force -y -out "%file_work%" "%~2"
for /f "tokens=2,4,6,8,10 delims=:	" %%a in ('findstr /r /i /b /c:"zc:..zm:..zs:" "%png_log%"') do (set "zlib_memlevel=%%b" &set "zlib_strategy=%%c")
pngwolf --in="%file_work%" --out="%file_work%" --max-stagnate-time=0 --max-evaluations=1 --zlib-level=7 --zlib-strategy=!zlib_strategy! --zlib-memlevel=!zlib_memlevel! --7zip-mpass=2 1>nul 2>&1
)
if %png% equ 8 (
1>nul 2>&1 pngquant --speed 1 256 "%~2" -f -o "%temp_file%"
call :check_compare "%~2" "%temp_file%"
truepng -nz -md remove all -force -quiet -y -out "%file_work%" "%~2"
)
if %png% equ 9 (
truepng -f3 -i0 -g0 -nc -a9 -md remove all -l -quiet -force -y -out "%file_work%" "%~2"
)
deflopt -k -b "%file_work%" 1>nul 2>&1
if errorlevel 1 (call:save_error_log "%~f2" "can not read image" & goto:png_clean)
call:backup2 "%~f2" "%file_work%" "%~f3" || set "error_backup=1"
if %error_backup% neq 0 (call:save_error_log "%~f2" "can not read image" & goto:png_clean)
call:save_log "%~f3" %pngoptimized_size%
if %thread% equ 1 for %%a in ("%~f3") do (set /a "image_sizePNG+=%%~za" & set /a "total_sizePNG+=%pngoptimized_size%")
:png_clean
1>nul 2>&1 del /f /q "%file_work%" "%png_log%"
exit /b
:backup
if not exist "%~1" exit /b 2
if not exist "%~2" exit /b 3
if %~z2 equ 0 (if "%3" neq "" (1>nul 2>&1 del /f /q "%~2") & exit /b 4)
if %~z1 leq %~z2 (
if "%3" neq "" (1>nul 2>&1 del /f /q "%~2")
) else (
1>nul 2>&1 copy /b /y "%~2" "%~1" || exit /b 1
if "%3" neq "" 1>nul 2>&1 del /f /q "%~2"
)
exit /b
:backup2
if not exist "%~1" exit /b 2
if not exist "%~2" exit /b 3
set "cone="
if %~z2 equ 0 set "cone=yes"
if %~z1 leq %~z2 set "cone=yes"
if defined cone (
if "%~1" neq "%~3" (1>nul 2>&1 copy /b /y "%~1" "%~3" || exit /b 1)
) else (
1>nul 2>&1 copy /b /y "%~2" "%~3" || exit /b 1
)
exit /b 0
:check_compare
if %~z2 leq %~z1 (
1>nul 2>&1 del /f /q %1
1>nul 2>&1 move /y %2 %1
)
exit /b
:save_log
set /a "change_size=%~z1-%2"
set /a "change_purcent=%change_size%*100/%2" 2>nul
set /a "fraction=%change_size%*100%%%2*100/%2" 2>nul
set /a "change_purcent=%change_purcent%*100+%fraction%"
call:division change_purcent 100 100
>>"%log_file2%" echo.%~1;%2;%~z1;%change_size%;%change_purcent%;ok
if %thread% equ 1 (
call:echo_file_info "%~1" %2 %~z1 %change_size% %change_purcent%
)
exit /b
:division
set "sign="
1>nul 2>&1 set /a "int=!%1!/%2"
1>nul 2>&1 set /a "fractiond=!%1!*%3/%2%%%3"
if "%fractiond:~,1%" equ "-" (set "sign=-" & set "fractiond=%fractiond:~1%")
1>nul 2>&1 set /a "fractiond=%3+%fractiond%"
if "%int:~,1%" equ "-" set "sign="
set "%1=%sign%%int%.%fractiond:~1%"
exit /b
:save_error_log
if exist "%file_work%" 1>nul 2>&1 del /f /q "%file_work%"
>>"%log_file2%" echo.%~1;%~2;error
if %thread% equ 1 (call:echo_file_error "%~f1" "%~2")
exit /b
:set_operation
call:set_time finish_time
set "fraction=0"
set "change_sizePNG=0" & set "change_purcentPNG=0"
if %png% equ 0 1>nul 2>&1 ping -n 1 -w 500 127.255.255.255 & goto:end_processing
if %thread% gtr 1 (
for /f "tokens=1-5 delims=;" %%a in ('findstr /e /i /r /c:";ok" "%log_file%png*" ') do (
set /a "total_sizePNG+=%%b" & set /a "image_sizePNG+=%%c"
)
)
set /a "change_sizePNG=(%total_sizePNG%-%image_sizePNG%)" 2>nul
set /a "change_purcentPNG=%change_sizePNG%*100/%total_sizePNG%" 2>nul
set /a "fraction=%change_sizePNG%*100%%%total_sizePNG%*100/%total_sizePNG%" 2>nul
set /a "change_purcentPNG=%change_purcentPNG%*100+%fraction%" 2>nul
call:division change_sizePNG 1024 100
call:division change_purcentPNG 100 100
:end_processing
title Finished - %name% - %version%
if %auto_close% equ 1 exit
:show_results
if "%total_all%" neq "0" (
1>&2 echo.
call:echo_script " Total : %change_sizePNG% KB (%%change_purcentPNG%%%%%%) saved."
1>&2 echo.
1>&2 echo.
call:echo_script " Started  at : %start_time%"
call:echo_script " Finished at : %finish_time%"
1>&2 echo.
1>&2 echo.
)
exit /b
:cleaning
1>&2 echo.
if exist "%temp%\%name%" 1>nul 2>&1 rd /s /q "%temp%\%name%"
exit
:how_to
title %name% - %version%
color 0f
1>&2 (
echo.
echo.
echo.
echo. HOW TO USE:
echo  ----------
echo.
echo  Drag-and-drop files/folders on %name%.cmd file
echo.
echo.
echo  ______________________________________________________________________________
echo.
echo.
echo.
echo. LIMITATIONS:
echo  -----------
echo.
echo  %name% does not support some characters.
echo.
echo  Use simple path/name like "C:\images\image.png"
echo. 
echo. 
echo. 
)
if exist "%temp%\%name%" 1>nul 2>&1 rd /s /q "%temp%\%name%"
call:script_pause
exit /b
:script_pause
set "x=%~f0"
echo.%cmdcmdline% | 1>nul 2>&1 findstr /ilc:"%x%" && 1>nul 2>&1 pause
set "x="
exit /b